import {Component} from 'angular2/core';
import {NgForm} from 'angular2/common';
import {biodataService} from './biodata.service';
import {BiodataSet} from './mock-biodata-list';
import {biodata} from './biodata';

@Component({
    selector:'biodata-form',
    template: `
        <h3>Biodata Form:</h3>
        <form #biodataForm="ngForm">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" required
                    [(ngModel)]="model.name"
                        ngControl="name" #name="ngForm">
                <div [hidden]="name.valid || name.pristine" class="alert alert-danger">Name is required</div>
            </div>
            <div class="form-group">
                <label for="alterEgo">Contact Number</label>
                <input type="text" class="form-control" [(ngModel)]="model.phone" maxlength="10">
            </div>
            <div class="form-group">
                <label for="power">Qualification</label>
                <select class="form-control" [(ngModel)]="model.qualification">
                    <option *ngFor="#p of qualifyList" [value]="p">{{p}}</option>
                </select>
            </div>
            <button type="submit" class="btn btn-default" (click)="saveBiodata(model)" [disabled]="!biodataForm.form.valid">Submit</button>
        </form>


        <h3>Live view of the form data:</h3>
        <div class="row">
            <div class="col-xs-3">Name</div>
            <div class="col-xs-9  pull-left">{{ model.name }}</div>
        </div>
        <div class="row">
            <div class="col-xs-3">Alter Ego</div>
            <div class="col-xs-9 pull-left">{{ model.phone }}</div>
        </div>
        <div class="row">
            <div class="col-xs-3">Power</div>
            <div class="col-xs-9 pull-left">{{ model.qualification }}</div>
        </div>
    `
})

export class biodataFormComponent {
    qualifyList = ['Be/B.Tech', 'M.Tech', 'BA/BSc', 'MA/MSc','MCA'];// this  is list of qualification select box
    
    constructor(private _biodataService: biodataService) {}
    
    model = {};//create an instance of the class

    saveBiodata(model) {
        this._biodataService.saveNewData(model);
        this.model = {};
     }
}