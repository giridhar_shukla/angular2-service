import {biodata} from './biodata';

export const BiodataSet: biodata[] = [
    {name: 'Giridhar Shukla', phone: '9876543210', qualification: 'B.Tech'},
    {name: 'Richa Shukla', phone: '8795461254', qualification: 'B.Sc.'}
];