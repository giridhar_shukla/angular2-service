import {Component} from 'angular2/core';
import {biodataFormComponent} from './biodata-form.component';
import {biodataService} from './biodata.service';
import {BiodataSet} from './mock-biodata-list';
import {biodata} from './biodata';
import {OnInit} from 'angular2/core'; 

@Component({
    selector: 'biodata-list',
    template: `
        <h3>Biodata list:</h3>
        <div *ngFor="#data of AllBiodata">
            <div class="row">
                <div class="col-xs-3">Name</div>
                <div class="col-xs-9  pull-left">{{ data.name }}</div>
            </div>
            <div class="row">
                <div class="col-xs-3">Phone</div>
                <div class="col-xs-9 pull-left">{{ data.phone }}</div>
            </div>
            <div class="row">
                <div class="col-xs-3">Qualification</div>
                <div class="col-xs-9 pull-left">{{ data.qualification }}</div>
            </div>
            <br/>
        </div>
        <br>
        <biodata-form></biodata-form>
    `,
    directives: [biodataFormComponent],
    providers: [biodataService]
})

export class BiodataListComponent implements OnInit {
    public AllBiodata: biodata[];
    
    constructor(private _biodataService: biodataService) {}
    
    getAllBiodata() {
        this._biodataService.getList().then((BiodataSet: biodata[])=>this.AllBiodata = BiodataSet);
    }
    ngOnInit() {
        this.getAllBiodata();
    }
}