import {Injectable} from 'angular2/core';
import {BiodataSet} from './mock-biodata-list';

@Injectable()

export class biodataService {
    getList() {
        return Promise.resolve(BiodataSet);
    }
    saveNewData(data) {
        BiodataSet.push(data);
    }
}