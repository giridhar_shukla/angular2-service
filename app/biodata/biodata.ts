export interface biodata {
    id: number,
    name: string,
    phone: string,
    qualification: string
}